/*
 * HashMap.cpp
 *
 *  Created on: May 4, 2020
 *      Author: 13027
 */


#include "HashMap.hpp"
#include "HashNode.hpp"
#include <iostream>
#include <cstring>
#include <cmath>

HashMap::HashMap(bool hash1_flag, bool coll1_flag) {
	numKeys = 0;
	mapSize = 11;
	collfn = coll1_flag;
	hashfn = hash1_flag;
	collisions = 0;
	hashcoll = 0;

	map = new HashNode*[mapSize];
	for (int i = 0; i < mapSize; i++) {
		map[i] = nullptr;
	}
}

/**
 * Places a key and its value in the map
 */
int HashMap::addKeyValue(std::string k, std::string v) {
	int idx = getIndex(k);

	map[idx] = new HashNode(k, v);

	if (numKeys < 1) {
		first = map[idx]->keyword;
	}
	numKeys += 1;
	return idx;
}

/**
 * Finds a viable index to place a key in the map
 */
int HashMap::getIndex(std::string k) {
	if (((float) numKeys + 1.0f) / ((float) mapSize) > 0.7f) {
		reHash();
	}

	int n_coll = 0;
	int keyfound = -1;
	if (hashfn) {
		keyfound = findKey(k, calcHash1(k), &n_coll);
	} else {
		keyfound = findKey(k, calcHash2(k), &n_coll);
	}

	if (n_coll > 0) {
		collisions += n_coll;
		hashcoll += 1;
	}

	if (keyfound < 0 || k == map[keyfound]->keyword) {
		int hash = ((hashfn) ? calcHash1(k) : calcHash2(k)) % mapSize;
		return ((collfn) ? coll1(hash, n_coll) : coll2(hash, n_coll));
	} else {
		if (collfn) {
			//n_coll += 1;
			return coll1(keyfound, n_coll);
		} else {
			//n_coll += 1;
			return coll2(keyfound, n_coll);
		}
	}
}

/**
 * This is a 32-bit string hasher based on the FNV-1a hashing algorithm (fast and very low collision rate).
 */
unsigned int HashMap::calcHash1(std::string k) {
	const unsigned int offset = 0x811c9dc5; // 32 bit FNV offset
	unsigned int hash = offset;

	int len = k.length();
	const char* cstr = k.c_str();

	for (int i = 0; i < len; i++) {
		hash ^= cstr[i];
		hash *= 0x01000193; // 32 bit FNV prime
	}

	return hash;
}

/**
 * Alternate power hashing function (slower, but should also have few collisions).
 */
unsigned int HashMap::calcHash2(std::string k) {
	int hash = 0;
	int len = k.length();
	const char* cstr = k.c_str();

	for (int i = 0; i < len; i++) {
		hash += pow(cstr[i], i);
	}

	return hash;
}

/**
 * This will not work with a mapSize less than 2, will never output a value less than 3
 * Returns a prime integer at least double the previous map size. 
 */
int HashMap::getNextPrime() {
	int n = mapSize * 2;
	for (int i = ((n % 2 == 0) ? n + 1 : n); ; i += 2) { // Jump by 2 because all primes will be odd, cuts calculation time in half
		int j = 3;
		for (; i % j != 0 && j < i; j++) { // Iterate until all divisors are exhausted or until the number is determined to be composite
			continue;
		}
		if (j >= i) {
			n = i;
			return i;
		}
	}
}

void HashMap::reHash() {
	int oldSize = mapSize;
	HashNode** oldmap = new HashNode*[oldSize];
	for (int i = 0; i < oldSize; i++) {
		oldmap[i] = map[i];
	}
	mapSize = getNextPrime();
	map = new HashNode*[mapSize];
	for (int i = 0; i < mapSize; i++) {
		map[i] = nullptr;
	}

	for (int i = 0; i < oldSize; i++) {
		HashNode* currNode = oldmap[i];
		if (currNode != nullptr) {
			int newidx = -1;
			if (currNode->currSize >= 1) {
				newidx = addKeyValue(currNode->keyword, currNode->values[0]);
			}

			if (currNode->currSize > 1 && newidx >= 0) {
				for (int i = 1; i < currNode->valuesSize; i++) {
					map[newidx]->addValue(currNode->values[i]);
				}
			}
		}
	}

	delete[] oldmap;
}

/**
 * Recursion safe quadratic probing collision handling method
 * h - valid hashed index of object k
 * i - number of collisions for this object
 * 
 * Returns new valid hashed index for k.
 * Inlined to prevent unnecessary inflation of executable.
 */
inline unsigned int HashMap::coll1(unsigned int h, int i) {
	return (h + (int) pow(i, 2)) % mapSize;
}

/**
 * Chaining collision handling method
 * h - valid hashed index of object k
 * i - number of collisions for this object
 * 
 * Returns h + i (linear probing). This is probably going to be slow.
 * Inlined to prevent unnecessary inflation of executable.
 */
inline unsigned int HashMap::coll2(unsigned int h, int i) {
	return (h + i) % mapSize;
}

/**
 * Finds the index of a key in the hashmap
 */
int HashMap::findKey(std::string k) {
	int coll = 0;
	if (hashfn) {
		return findKey(k, calcHash1(k), &coll);
	} else {
		return findKey(k, calcHash2(k), &coll);
	}
}

/**
 * Recursive function for finding the key. Use findKey(std::string) outside of the class definitions.
 */
int HashMap::findKey(std::string k, unsigned int curr_hash, int* n_coll) {
	unsigned int idx = curr_hash % mapSize;
	if (map[idx] == nullptr) {
		return -1;
	} else {
		// HashNode* tmp = map[idx];
		if (map[idx]->keyword == k) {
			return idx;
		} else {
			if (collfn) {
				*n_coll += 1;
				return findKey(k, coll1(curr_hash, *n_coll), n_coll);
			} else {
				*n_coll += 1;
				return findKey(k, coll2(curr_hash, *n_coll), n_coll);
			}
		}
	}
}

void HashMap::printMap() {
	cout << "In printMap()" << endl;
	for (int i = 0; i < mapSize; i++) {
		//cout << "In loop" << endl;
		if (map[i] != nullptr) {
			cout << map[i]->keyword << ": ";
			for (int j = 0; j < map[i]->currSize;j++) {
				cout << map[i]->values[j] << ", ";
			}
			cout << endl;
		}
	}
}
