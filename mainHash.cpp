/*
 * mainHash.cpp
 *
 *  Created on: May 4, 2020
 *      Author: 13027
 */


#include <iostream>
#include "MakeSeuss.hpp"
#include <ctime>
#include <cstdlib>

int main() {
	srand(time(NULL));

	MakeSeuss k("DrSeuss.txt","Seussout.txt",true,true);
	MakeSeuss m("GEChap1a.txt","GEChap1out.txt",false,true);

	MakeSeuss n("DrSeuss.txt","Seussout2.txt",true,false);
	MakeSeuss v("GEChap1a.txt","GEChap1out2.txt",false,false);
}
