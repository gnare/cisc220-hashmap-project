/*
 * MakeSeuss.hpp
 *
 *  Created on: May 4, 2020
 *      Author: 13027
 */

#ifndef MAKESEUSS_HPP_
#define MAKESEUSS_HPP_

	#include "HashMap.hpp"
	#include <iostream>

	class MakeSeuss {
		HashMap *ht;
		std::string fn;
		std::string newfile;
		bool hashfn;
		bool collfn;
	public:
		MakeSeuss(std::string file, std::string newf, bool hash1, bool coll1);
		void readFile();
		void writeFile();

	};

#endif /* MAKESEUSS_HPP_ */
